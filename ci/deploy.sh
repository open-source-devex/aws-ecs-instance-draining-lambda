#!/usr/bin/env bash

./gradlew shadowJar

aws lambda create-function \
  --publish --runtime java8 --region eu-central-1 \
  --timeout 120 --memory-size 384 \
  --function-name Drainer \
  --description "ECS instance draining function" \
  --zip-file fileb://build/libs/ecs-instance-draining-1.0-SNAPSHOT-aws.jar  \
  --role arn:aws:iam::415033069301:role/lambda-ecs-instance-draining-role \
  --handler org.springframework.cloud.function.adapter.aws.SpringBootStreamHandler
