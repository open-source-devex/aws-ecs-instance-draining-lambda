#!/usr/bin/env bash

./gradlew shadowJar
aws lambda update-function-code \
  --publish --region eu-central-1 \
  --function-name Drainer \
  --zip-file fileb://build/libs/ecs-instance-draining-1.0-SNAPSHOT-aws.jar
