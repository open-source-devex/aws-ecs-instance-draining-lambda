package com.gitlab.osdevex.ecs.instancedraining;

import java.util.function.Function;

import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.osdevex.ecs.instancedraining.aws.AwsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    @Value("${application.wait.afterDrainCommand:60}")
    private int afterDrainCommand;
    @Value("${application.wait.beforeReTriggering:30}")
    private int beforeReTriggering;
    @Value("${LIFECYCLE_HOOK_NAME:instance-draining}")
    private String lifecycleHookName;

    @Autowired
    private ObjectMapper objectMapper;

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public ObjectMapper objectMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

        return mapper;
    }

    private SNSEvent printEvent(final SNSEvent event) {
        try {
            logger.info("Got event:\n" + objectMapper.writeValueAsString(event));
        } catch (final JsonProcessingException e) {
            logger.error("Could not print event.", e);
        }
        return event;
    }

    @Bean
    SnsEventHandler snsEventHandler(final ObjectMapper mapper, final AwsService awsService) {
        return new SnsEventHandler(mapper, awsService, afterDrainCommand, beforeReTriggering, lifecycleHookName);
    }

    @Bean
    Function<Flux<SNSEvent>, Flux<SNSEvent>> drainer(final SnsEventHandler snsEventHandler) {
        return (Flux<SNSEvent> flux) -> flux.map(this::printEvent).map(snsEvent -> handleEvent(snsEventHandler, snsEvent));
    }

    private SNSEvent handleEvent(final SnsEventHandler snsEventHandler, final SNSEvent snsEvent) {
        snsEventHandler.handleEvent(snsEvent);
        return snsEvent;
    }
}
