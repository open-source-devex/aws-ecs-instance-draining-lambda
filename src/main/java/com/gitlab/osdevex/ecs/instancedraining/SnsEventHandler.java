package com.gitlab.osdevex.ecs.instancedraining;

import static com.gitlab.osdevex.ecs.instancedraining.SnsEventValidator.FIRST_EVENT_RECORD_MESSAGE_WAS_NOT_OF_CORRECT_TYPE;
import static com.gitlab.osdevex.ecs.instancedraining.SnsEventValidator.IO_EXCEPTION_CAUGHT_UNMARSHALLING_IN_MEMORY_STRING;
import static com.gitlab.osdevex.ecs.instancedraining.SnsEventValidator.NOTIFICATION_METADATA_WAS_NOT_OF_CORRECT_TYPE;

import java.io.IOException;

import com.amazonaws.services.ecs.model.ContainerInstance;
import com.amazonaws.services.ecs.model.ContainerInstanceStatus;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.osdevex.ecs.instancedraining.aws.AwsService;
import com.gitlab.osdevex.ecs.instancedraining.aws.DrainInstanceException;
import com.gitlab.osdevex.ecs.instancedraining.aws.EcsClusterNameNotFoundException;
import com.gitlab.osdevex.ecs.instancedraining.aws.InstanceNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class SnsEventHandler {

    private static final Logger logger = LoggerFactory.getLogger(SnsEventHandler.class);

    private final ObjectMapper mapper;
    private final SnsEventValidator snsEventValidator;
    private final AwsService awsService;
    private final int afterDrainCommand;
    private final int beforeReTriggering;
    private final String lifecycleHookName;

    @Autowired
    public SnsEventHandler(@NotNull final ObjectMapper mapper, @NotNull final AwsService awsService, final int afterDrainCommand, final int beforeReTriggering,
                           @NotNull final String lifecycleHookName) {
        this.mapper = mapper;
        this.snsEventValidator = new SnsEventValidator(mapper);
        this.awsService = awsService;
        this.afterDrainCommand = afterDrainCommand;
        this.beforeReTriggering = beforeReTriggering;
        this.lifecycleHookName = lifecycleHookName;
    }

    public void handleEvent(final SNSEvent event) {
        logger.info("Handling SNS Event");
        snsEventValidator.validateEvent(event, lifecycleHookName);

        final SNSEvent.SNS sns = getSns(event);
        final String topicArn = getTopicArn(sns);
        final String message = getMessage(sns);

        final SnsEventLifecycleHookMessage lifecycleHook = unmarshallLifecycleHook(message);
        handleLifecycleChange(topicArn, message, lifecycleHook);
    }

    private void handleLifecycleChange(final String topicArn, final String message, final SnsEventLifecycleHookMessage lifecycleHook) {
        final String ec2InstanceId = lifecycleHook.getEc2InstanceId();
        final SnsNotificationMetadata notificationMetadata = unmarshallNotificationMetaData(lifecycleHook.getNotificationMetadata());
        final String lifecycleHookName = lifecycleHook.getLifecycleHookName();
        final String autoScalingGroupName = lifecycleHook.getAutoScalingGroupName();

        try {
            final String ecsCluster = awsService.findEcsClusterName(ec2InstanceId);
            final ContainerInstance containerInstance = awsService.containerInstance(ecsCluster, ec2InstanceId);
            final ContainerInstanceStatus containerInstanceStatus = ContainerInstanceStatus.fromValue(containerInstance.getStatus());
            switch (containerInstanceStatus) {
                case DRAINING:
                    final int taskCount = countTasksRunningOnInstance(ecsCluster, containerInstance.getContainerInstanceArn());
                    if (isDrainedInstance(notificationMetadata, taskCount)) {
                        continueLifecycle(ec2InstanceId, lifecycleHookName, autoScalingGroupName);
                    } else {
                        waitBeforeCheckingInstanceState();
                        pushTriggerEvent(message, topicArn);
                    }
                    break;
                case ACTIVE:
                    drainInstance(ecsCluster, containerInstance);
                    waitAfterDrainCommand();
                    pushTriggerEvent(message, topicArn);
                    break;
            }
        } catch (final EcsClusterNameNotFoundException e) {
            logger.warn("Could not find cluster name.", e);
            abortLifecycle(ec2InstanceId, lifecycleHookName, autoScalingGroupName);
        } catch (final InstanceNotFoundException e) {
            logger.warn("Could not find container instance.", e);
            abortLifecycle(ec2InstanceId, lifecycleHookName, autoScalingGroupName);
        } catch (final DrainInstanceException e) {
            e.getFailures().forEach(failure -> logger.warn("Failure setting instance to DRAINING state: " + failure));
            abortLifecycle(ec2InstanceId, lifecycleHookName, autoScalingGroupName);
        }
    }

    private void waitBeforeCheckingInstanceState() {
        logger.info("Waiting {} seconds before re-triggering", beforeReTriggering);
        waitForSeconds(beforeReTriggering);
    }

    private void waitAfterDrainCommand() {
        logger.info("Waiting {} seconds after drain command was sent", afterDrainCommand);
        waitForSeconds(afterDrainCommand);
    }

    private void waitForSeconds(final int waitAfterDrainCommandSeconds) {
        try {
            Thread.sleep(waitAfterDrainCommandSeconds * 1000);
        } catch (final InterruptedException e) {
            logger.warn("Waiting period interrupted", e);
        }
    }

    private boolean isDrainedInstance(final SnsNotificationMetadata notificationMetadata, final int taskCount) {
        final int ecsClusterDaemonTasksCount = notificationMetadata.getEcsInstanceDaemonTasksCount();
        final boolean isDrained = taskCount <= ecsClusterDaemonTasksCount;
        logger.info("Instance is{} drained because it runs {} tasks of which {} should be daemons", isDrained ? "" : " not", taskCount, ecsClusterDaemonTasksCount);
        return isDrained;
    }

    private void pushTriggerEvent(final String topicArn, final String message) {
        final String messageId = awsService.pushSnsNotification(message, topicArn, "Pushing back SNS message to re-trigger lambda");
        logger.info("Pushed re-trigger SNS message {}", messageId);
    }

    private int countTasksRunningOnInstance(final String ecsCluster, final String containerInstanceId) {
        final int count = awsService.countTasksRunningOnInstance(ecsCluster, containerInstanceId);
        logger.debug("Found {} tasks running on instance {}", count, containerInstanceId);
        return count;
    }

    private void drainInstance(final String clusterName, final ContainerInstance containerInstance) throws DrainInstanceException {
        logger.info("Sending drain instance command");
        awsService.drainInstance(clusterName, containerInstance);
        logger.info("Drain instance command sent");
    }

    private void continueLifecycle(@NotNull final String instanceId, @NotNull final String lifecycleHookName, @NotNull final String autoScalingGroupName) {
        logger.info("Sending continue lifecycle command");
        awsService.continueLifecycle(lifecycleHookName, autoScalingGroupName, instanceId);
        logger.info("Continue lifecycle command sent");
    }

    private String getMessage(final SNSEvent.SNS snsRecord) {
        return snsRecord.getMessage();
    }

    private String getTopicArn(final SNSEvent.SNS snsRecord) {
        return snsRecord.getTopicArn();
    }

    private SNSEvent.SNS getSns(final @NotNull SNSEvent event) {
        return event.getRecords().get(0).getSNS();
    }

    private void abortLifecycle(@NotNull final String instanceId, @NotNull final String lifecycleHookName, @NotNull final String autoScalingGroupName) {
        awsService.abortLifecycle(lifecycleHookName, autoScalingGroupName, instanceId);
        logger.info("Aborting lifecycle command sent");
    }

    private SnsEventLifecycleHookMessage unmarshallLifecycleHook(final String message) {
        try {
            return mapper.readValue(message, SnsEventLifecycleHookMessage.class);
        } catch (JsonParseException | JsonMappingException e) {
            throw new IllegalArgumentException(FIRST_EVENT_RECORD_MESSAGE_WAS_NOT_OF_CORRECT_TYPE, e);
        } catch (final IOException e) {
            throw new RuntimeException(IO_EXCEPTION_CAUGHT_UNMARSHALLING_IN_MEMORY_STRING, e);
        }
    }

    private SnsNotificationMetadata unmarshallNotificationMetaData(final String notificationMetadata) {
        try {
            return mapper.readValue(notificationMetadata, SnsNotificationMetadata.class);
        } catch (JsonParseException | JsonMappingException e) {
            throw new IllegalArgumentException(NOTIFICATION_METADATA_WAS_NOT_OF_CORRECT_TYPE, e);
        } catch (final IOException e) {
            throw new RuntimeException(IO_EXCEPTION_CAUGHT_UNMARSHALLING_IN_MEMORY_STRING, e);
        }
    }
}
