package com.gitlab.osdevex.ecs.instancedraining.aws;

public class EcsClusterNameNotFoundException extends Exception {
    EcsClusterNameNotFoundException(final String message) {
        super(message);
    }
}
