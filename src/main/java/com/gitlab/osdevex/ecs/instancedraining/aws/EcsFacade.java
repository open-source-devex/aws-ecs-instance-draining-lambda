package com.gitlab.osdevex.ecs.instancedraining.aws;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.model.ContainerInstance;
import com.amazonaws.services.ecs.model.ContainerInstanceStatus;
import com.amazonaws.services.ecs.model.DescribeContainerInstancesRequest;
import com.amazonaws.services.ecs.model.DescribeContainerInstancesResult;
import com.amazonaws.services.ecs.model.Failure;
import com.amazonaws.services.ecs.model.ListContainerInstancesRequest;
import com.amazonaws.services.ecs.model.ListTasksRequest;
import com.amazonaws.services.ecs.model.UpdateContainerInstancesStateRequest;
import com.amazonaws.services.ecs.model.UpdateContainerInstancesStateResult;
import io.vavr.collection.Stream;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class EcsFacade {

    private static final Logger logger = LoggerFactory.getLogger(EcsFacade.class);

    @NotNull
    private final AmazonECS ecsClient;

    EcsFacade(@NotNull final AmazonECS ecsClient) {
        this.ecsClient = ecsClient;
    }

    ContainerInstance containerInstance(@NotNull final String clusterName, @NotNull final String ec2InstanceId) throws InstanceNotFoundException {
        final Collection<String> containerInstances = listContainerInstances(clusterName);
        final DescribeContainerInstancesResult describeContainerInstancesResult = describeContainerInstances(clusterName, containerInstances);
        return Stream.ofAll(describeContainerInstancesResult.getContainerInstances())
                     .filter(instance -> instance.getEc2InstanceId().equals(ec2InstanceId))
                     .headOption()
                     .getOrElseThrow(() -> buildException(clusterName, ec2InstanceId));
    }

    private Collection<String> listContainerInstances(final @NotNull String clusterName) {
        final ListContainerInstancesRequest request = new ListContainerInstancesRequest();
        request.setCluster(clusterName);
        return ecsClient.listContainerInstances(request).getContainerInstanceArns();
    }

    private DescribeContainerInstancesResult describeContainerInstances(final @NotNull String clusterName, final Collection<String> containerInstances) {
        final DescribeContainerInstancesRequest describeContainerInstancesRequest = new DescribeContainerInstancesRequest();
        describeContainerInstancesRequest.setCluster(clusterName);
        describeContainerInstancesRequest.setContainerInstances(containerInstances);
        return ecsClient.describeContainerInstances(describeContainerInstancesRequest);
    }

    @NotNull
    private InstanceNotFoundException buildException(final @NotNull String clusterName, final @NotNull String ec2InstanceId) {
        return new InstanceNotFoundException("Could not find ECS container instance that matches EC2 instance " + ec2InstanceId + " in cluster " + clusterName);
    }

    List<String> ecsTasksRunningOnInstance(final @NotNull String clusterName, final @NotNull String containerInstanceId) {
        final ListTasksRequest request = new ListTasksRequest();
        request.setCluster(clusterName);
        request.setContainerInstance(containerInstanceId);
        return ecsClient.listTasks(request).getTaskArns();
    }

    int countTasksRunningOnInstance(final @NotNull String clusterName, final @NotNull String containerInstanceId) {
        return ecsTasksRunningOnInstance(clusterName, containerInstanceId).size();
    }

    void drainInstance(final String clusterName, final ContainerInstance containerInstance) throws DrainInstanceException {
        final UpdateContainerInstancesStateRequest request = new UpdateContainerInstancesStateRequest();
        request.setCluster(clusterName);
        request.setContainerInstances(Collections.singletonList(containerInstance.getContainerInstanceArn()));
        request.setStatus(ContainerInstanceStatus.DRAINING);
        final UpdateContainerInstancesStateResult updateContainerInstancesStateResult = ecsClient.updateContainerInstancesState(request);
        final List<Failure> failures = updateContainerInstancesStateResult.getFailures();
        if (!failures.isEmpty()) {
            throw new DrainInstanceException(failures);
        }
    }
}
