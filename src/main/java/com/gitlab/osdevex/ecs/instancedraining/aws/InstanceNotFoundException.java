package com.gitlab.osdevex.ecs.instancedraining.aws;

public class InstanceNotFoundException extends Exception {
    InstanceNotFoundException(final String message) {
        super(message);
    }
}
