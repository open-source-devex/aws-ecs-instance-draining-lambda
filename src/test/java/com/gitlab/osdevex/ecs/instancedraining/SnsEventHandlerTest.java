package com.gitlab.osdevex.ecs.instancedraining;

import static com.gitlab.osdevex.ecs.instancedraining.TestConstants.CLUSTER_NAME;
import static com.gitlab.osdevex.ecs.instancedraining.TestConstants.EC2_INSTANCE_ID;
import static com.gitlab.osdevex.ecs.instancedraining.TestConstants.EVENT;
import static com.gitlab.osdevex.ecs.instancedraining.TestConstants.LIFECYCLE_HOOK_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.amazonaws.services.ecs.model.ContainerInstance;
import com.amazonaws.services.ecs.model.ContainerInstanceStatus;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.osdevex.ecs.instancedraining.aws.AwsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SnsEventHandlerTest {

    private ObjectMapper mapper;
    private final AwsService awsService = mock(AwsService.class);

    @BeforeEach
    void setUp() {
        mapper = TestUtils.getObjectMapper();
    }

    @Test
    void unmarshallEvent() throws Exception {
        final SNSEvent snsEvent = mapper.readValue(EVENT, SNSEvent.class);

        assertThat(snsEvent).isNotNull()
                            .extracting(SNSEvent::getRecords).hasSize(1);
    }

    @Test
    void handleEvent_whenInstanceIsActive() throws Exception {
        final ContainerInstance containerInstance = new ContainerInstance();
        containerInstance.setStatus(ContainerInstanceStatus.ACTIVE.toString());
        when(awsService.findEcsClusterName(EC2_INSTANCE_ID)).thenReturn(CLUSTER_NAME);
        when(awsService.containerInstance(CLUSTER_NAME, EC2_INSTANCE_ID)).thenReturn(containerInstance);

        new SnsEventHandler(mapper, awsService, 1, 1, LIFECYCLE_HOOK_NAME).handleEvent(mapper.readValue(EVENT, SNSEvent.class));
    }
}
