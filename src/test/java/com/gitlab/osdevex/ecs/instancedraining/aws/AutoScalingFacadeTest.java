package com.gitlab.osdevex.ecs.instancedraining.aws;

import static org.mockito.Mockito.mock;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import org.junit.jupiter.api.Test;

class AutoScalingFacadeTest {

    private final AmazonAutoScaling amazonAutoScalingClient = mock(AmazonAutoScaling.class);

    @Test
    void abortLifecycle() throws Exception {
        final String lifecycleHookName = "hookName";
        final String autoScalingGroupName = "groupName";
        final String instanceId = "instanceId";

        new AutoScalingFacade(amazonAutoScalingClient).abortLifecycle(lifecycleHookName, autoScalingGroupName, instanceId);
    }

    @Test
    void continueLifecycle() throws Exception {
        final String lifecycleHookName = "hookName";
        final String autoScalingGroupName = "groupName";
        final String instanceId = "instanceId";

        new AutoScalingFacade(amazonAutoScalingClient).continueLifecycle(lifecycleHookName, autoScalingGroupName, instanceId);
    }
}
