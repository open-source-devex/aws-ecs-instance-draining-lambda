package com.gitlab.osdevex.ecs.instancedraining.aws;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstanceAttributeRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceAttributeResult;
import com.amazonaws.services.ec2.model.InstanceAttribute;
import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.sns.AmazonSNS;
import org.junit.jupiter.api.Test;

class AwsServiceTest {

    private static final String INSTANCE_ID = "instanceId";
    private final AmazonEC2 amazonEC2 = mock(AmazonEC2.class);
    private final Ec2Facade ec2Client = new Ec2Facade(amazonEC2);
    private final EcsFacade ecsClient = new EcsFacade(mock(AmazonECS.class));
    private final AutoScalingFacade autoScalingClient = new AutoScalingFacade(mock(AmazonAutoScaling.class));
    private final SnsFacade snsClient = new SnsFacade(mock(AmazonSNS.class));

    @Test
    void findEcsClusterName_whenClusterNameIsNotFound() throws Exception {
        final DescribeInstanceAttributeRequest request = new DescribeInstanceAttributeRequest(INSTANCE_ID, Ec2Facade.USER_DATA_ATTRIBUTE);
        final InstanceAttribute instanceAttribute = new InstanceAttribute();
        instanceAttribute.setUserData("");
        final DescribeInstanceAttributeResult result = new DescribeInstanceAttributeResult();
        result.setInstanceAttribute(instanceAttribute);
        when(amazonEC2.describeInstanceAttribute(request)).thenReturn(result);

        assertThatExceptionOfType(EcsClusterNameNotFoundException.class).isThrownBy(
                () -> new AwsService(ec2Client, ecsClient, autoScalingClient, snsClient).findEcsClusterName(INSTANCE_ID)
        );
    }
}
