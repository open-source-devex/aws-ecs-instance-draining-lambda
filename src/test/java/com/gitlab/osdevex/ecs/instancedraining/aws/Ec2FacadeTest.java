package com.gitlab.osdevex.ecs.instancedraining.aws;

import static com.gitlab.osdevex.ecs.instancedraining.aws.Ec2Facade.USER_DATA_ATTRIBUTE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.DescribeInstanceAttributeRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceAttributeResult;
import com.amazonaws.services.ec2.model.InstanceAttribute;
import org.junit.jupiter.api.Test;

class Ec2FacadeTest {

    private static final String INSTANCE_DOES_NOT_EXIST_ERROR =
            "The instance ID 'i-04f5c07da28fe80c4' does not exist (Service: AmazonEC2; Status Code: 400;";
    private static final String THIS_IS_A_TEST_BASE64 = "VGhpcyBpcyBhIHRlc3Q=";
    private static final String THIS_IS_A_TEST = "This is a test";
    private static final String INSTANCE_ID = "instanceId";

    private final AmazonEC2 ec2Client = mock(AmazonEC2.class);

    @Test
    void getInstanceUserData_whenInstanceExists() throws Exception {
        final InstanceAttribute instanceAttribute = new InstanceAttribute();
        instanceAttribute.setUserData(THIS_IS_A_TEST_BASE64);
        final DescribeInstanceAttributeResult describeInstanceAttributeResult = new DescribeInstanceAttributeResult();
        describeInstanceAttributeResult.setInstanceAttribute(instanceAttribute);
        when(ec2Client.describeInstanceAttribute(new DescribeInstanceAttributeRequest(INSTANCE_ID, USER_DATA_ATTRIBUTE))).thenReturn(describeInstanceAttributeResult);

        final String userData = new Ec2Facade(ec2Client).getInstanceUserData(INSTANCE_ID);

        assertThat(userData).isEqualTo(THIS_IS_A_TEST);
    }

    @Test
    void getInstanceUserData_whenInstanceDoesNotExist() throws Exception {
        final InstanceAttribute instanceAttribute = new InstanceAttribute();
        instanceAttribute.setUserData(THIS_IS_A_TEST_BASE64);
        final DescribeInstanceAttributeResult describeInstanceAttributeResult = new DescribeInstanceAttributeResult();
        describeInstanceAttributeResult.setInstanceAttribute(instanceAttribute);
        when(ec2Client.describeInstanceAttribute(new DescribeInstanceAttributeRequest(INSTANCE_ID, USER_DATA_ATTRIBUTE)))
                .thenThrow(new AmazonEC2Exception(INSTANCE_DOES_NOT_EXIST_ERROR));

        assertThatExceptionOfType(AmazonEC2Exception.class)
                .isThrownBy(() -> new Ec2Facade(ec2Client).getInstanceUserData(INSTANCE_ID))
                .withMessageStartingWith(INSTANCE_DOES_NOT_EXIST_ERROR);
    }
}
