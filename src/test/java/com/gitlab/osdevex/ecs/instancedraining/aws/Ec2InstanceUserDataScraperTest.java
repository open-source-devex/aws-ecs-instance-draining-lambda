package com.gitlab.osdevex.ecs.instancedraining.aws;

import static com.gitlab.osdevex.ecs.instancedraining.TestConstants.CLUSTER_NAME;
import static com.gitlab.osdevex.ecs.instancedraining.TestConstants.USER_DATA;
import static org.assertj.core.api.Assertions.assertThat;

import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

class Ec2InstanceUserDataScraperTest {

    @Test
    void scrapeEcsCluster_emptyUserData() throws Exception {
        final Ec2InstanceUserDataScraper ecsInstanceUserDataScraper = new Ec2InstanceUserDataScraper();

        final Option<String> maybeEcsCluster = ecsInstanceUserDataScraper.ecsCluster("");

        assertThat(maybeEcsCluster).isNotNull();
        assertThat(maybeEcsCluster.isDefined()).isFalse();
    }

    @Test
    void scrapeEcsCluster_expectedUserData() throws Exception {

        final Ec2InstanceUserDataScraper ecsInstanceUserDataScraper = new Ec2InstanceUserDataScraper();

        final Option<String> maybeEcsCluster = ecsInstanceUserDataScraper.ecsCluster(USER_DATA);

        assertThat(maybeEcsCluster).isNotNull();
        assertThat(maybeEcsCluster.isDefined()).isTrue();
        assertThat(maybeEcsCluster.get()).isEqualTo(CLUSTER_NAME);
    }

    @Test
    void ecsClusterRegex_cloudConfig() throws Exception {
        final Ec2InstanceUserDataScraper ecsInstanceUserDataScraper = new Ec2InstanceUserDataScraper();

        assertThat(ecsInstanceUserDataScraper.extractEcsClusterValue("  - echo 'ECS_CLUSTER=name01._2-' >> /etc/ecs/ecs.config")).isEqualTo("name01._2-");
        assertThat(ecsInstanceUserDataScraper.extractEcsClusterValue("  - echo 'ECS_CLUSTER=name01._2-' >> /etc/ecs/ecs.config\n")).isEqualTo("name01._2-");
        assertThat(ecsInstanceUserDataScraper.extractEcsClusterValue("  - echo \"ECS_CLUSTER=name01._2-\" >> /etc/ecs/ecs.config")).isEqualTo("name01._2-");
        assertThat(ecsInstanceUserDataScraper.extractEcsClusterValue("  - echo \"ECS_CLUSTER=name01._2-\" >> /etc/ecs/ecs.config\n")).isEqualTo("name01._2-");
    }

    @Test
    void ecsClusterRegex_shell() throws Exception {
        final Ec2InstanceUserDataScraper ecsInstanceUserDataScraper = new Ec2InstanceUserDataScraper();

        assertThat(ecsInstanceUserDataScraper.extractEcsClusterValue("echo \"ECS_CLUSTER=name\" >> /etc/ecs/ecs.config")).isEqualTo("name");
        assertThat(ecsInstanceUserDataScraper.extractEcsClusterValue("echo \"ECS_CLUSTER='name'\" >> /etc/ecs/ecs.config")).isEqualTo("name");
        assertThat(ecsInstanceUserDataScraper.extractEcsClusterValue("echo 'ECS_CLUSTER=name' >> /etc/ecs/ecs.config")).isEqualTo("name");
        assertThat(ecsInstanceUserDataScraper.extractEcsClusterValue("echo 'ECS_CLUSTER=\"name\"' >> /etc/ecs/ecs.config")).isEqualTo("name");
    }
}
